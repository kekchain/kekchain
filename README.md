
# KEKCHAIN
_____________________________________________________________________________________________________________________________________________________________________________________________

## THE decentralised red pill archiv
 
_____________________________________________________________________________________________________________________________________________________________________________________________

### Content

- What is Kekchain?
- How to install
- User guide
- KeK distribution
- Trouble shooting
- Patch notes

_____________________________________________________________________________________________________________________________________________________________________________________________

### KEKCHAIN

Kekchain - THE decentralised red pill archive

Kekchain is a decentralized peer to peer transaction and smart contract platform.
It supports QuantumresistantRedpillCommando Tokens (QRC Tokens) to archive and distribute red pills and critical informations.
For validating new transactions and blocks, a Proof of Stake (PoS) mechanism is used.
Everybody with the Kekchain core wallet can participate in this process and earn KeK on the way.

For instructions how to upload red pills onto the blockchain, read the qrcREADME.md inside the QRCToken folder.

Kek is the deification of the concept of primordial darkness (kkw sm3w) in the Ancient Egyptian Ogdoad cosmogony of Hermopolis.
The Ogdoad consisted of four pairs of deities, four male gods paired with their female counterparts. Kek's female counterpart was Kauket. Kek and Kauket in some aspects also represent night and day, and were called "raiser up of the light" and the "raiser up of the night", respectively.
The name is written as kk or kkwy with a variant of the sky hieroglyph in ligature with the staff (N2) associated with the word for "darkness" kkw.
In the oldest representations, Kekui is given the head of a serpent, and Kekuit the head of either a frog or a cat. In one scene, they are identified with Ka and Kait; in this scene, Ka-Kekui has the head of a frog surmounted by a beetle and Kait-Kekuit has the head of a serpent surmounted by a disk.
In the Greco-Roman period, Kek's male form was depicted as a frog-headed man, and the female form as a serpent-headed woman, as were all four dualistic concepts in the Ogdoad.

Tokenomics

- 8 KeK mined at genesis
- 88 KeK block reward, halving every 1.69 years
- 888888 blocks per halving cycle
- fixed 0.00000001 block reward after 34 halving
- 60 seconds block time
- difficulty adjustment every 10 minutes
- proof of stake algorithm since block 1501
- coins mature for staking in 500 blocks
- 10 blocks recommended confirmation time

_____________________________________________________________________________________________________________________________________________________________________________________________

### HOW TO INSTALL

#### Build on Ubuntu Linux (For other distros, you may need to change the commands slightly)

This is a quick start script for compiling Kekchain on Ubuntu Linux

    sudo apt-get install build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils git cmake libboost-all-dev libgmp3-dev -y
    sudo apt-get install software-properties-common -y
    sudo apt-get install libdb++-dev -y
    sudo apt-get install libminiupnpc-dev -y

    # If you want to build the Qt GUI:
    sudo apt-get install libqt5gui5 libqt5core5a libqt5dbus5 qttools5-dev qttools5-dev-tools libprotobuf-dev protobuf-compiler qrencode -y
    
    # Download the github content and navigate to your kekchain folder
    git clone https://github.com/kekchain-project/kekchain --recursive
    cd kekchain

    # Note autogen will prompt to install some more dependencies if needed. Incompatible bdb is used to not be dependant on the bitcoin PPA which is managed by one person.
    chmod u+x ./autogen.sh
    ./autogen.sh
    cd share
    chmod u+x ./genbuild.sh
    cd ..
    ./configure --with-incompatible-bdb
    make -j2

    # run it
    cd src/qt
    ./kekchain-qt
    # or update the kekchain-qt.desktop file (open it with a textfile to update it) and make it executable to get a launcher



If you trust the bitcoin PPA Launchpad, you can go this route

    sudo apt-get install build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils git cmake libboost-all-dev libgmp3-dev -y
    sudo apt-get install software-properties-common -y
    sudo add-apt-repository ppa:bitcoin/bitcoin
    sudo apt-get update -y
    sudo apt-get install libdb4.8-dev libdb4.8++-dev -y

    # If you want to build the Qt GUI:
    sudo apt-get install libqt5gui5 libqt5core5a libqt5dbus5 qttools5-dev qttools5-dev-tools libprotobuf-dev protobuf-compiler qrencode -y

    # Download the github content and navigate to your kekchain folder
    git clone https://github.com/kekchain-project/kekchain --recursive
    cd kekchain

    # Note autogen will prompt to install some more dependencies if needed
    chmod u+x ./autogen.sh
    ./autogen.sh
    cd share
    chmod u+x ./genbuild.sh
    cd ..
    ./configure
    make -j2

    # run it
    cd src/qt
    ./kekchain-qt
    # or update the kekchain-qt.desktop file (open it with a textfile to update it) and make it executable to get a launcher

    
#### Build on Windows

The easiest way to build from source on Windows would be to use a virtual box/machine and use a Linux distro there.
You can also search for "how to build bitcoin from source on windows" guides and adapt that to kekchain.


#### Install from .deb package:


Download the latest .deb package at https://github.com/kekchain-project/kekchain/releases

    #Install dependencies
    sudo apt-get install build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils git cmake libboost-all-dev libgmp3-dev -y
    sudo apt-get install software-properties-common -y
    sudo apt-get install libdb++-dev -y
    sudo apt-get install libminiupnpc-dev -y
    sudo apt-get install libqt5gui5 libqt5core5a libqt5dbus5 qttools5-dev qttools5-dev-tools libprotobuf-dev protobuf-compiler qrencode -y

    #Install the .deb package. It will be installed into /usr/local/lib/kekchain
    sudo dpkg -i /PATH/TO/filename.deb

    #If this fails with a message about the package depending on something that isn't installed, you can probably fix it if you follow up with this
    sudo apt-get update
    sudo apt-get -f install
    sudo dpkg -i /PATH/TO/filename.deb


#### Deinstall

- just delete the kekchain main folder

- if installed with .deb package, you can also $ sudo apt remove kekchain-core-wallet

_____________________________________________________________________________________________________________________________________________________________________________________________

### USER GUIDE

Use the wallet to send and receive KeK.

Create a passphrase for more security (settings->create/change passphrase)

Stake your mature KeK (kek matures after 500 blocks) to validate new blocks and transactions, earning KeK in the process. Unlock your wallet (settings->unlock wallet) and simply let it open to stake.

Use an ethereum solidity compiler to compile smart contracts and host the code on the blockchain. You can also use smart contracts to create your own token ontop of the kekchain, storing red pills that way. Read the qrcREADME.md for a step by step guide.

You can read the red pills inside the wallet at the QRC Token page, or "right click"->"Copy token redpill" and save it into a local text file.

Always create a wallet backup after creating a new wallet (file->backup wallet) You need to create a new backup every time you change your passphrase.

The kekchain core wallet is a HD wallet, that means new addresses will be created on the fly every time you need them.

A simple blockbrowser can be find at Help->Debugwindow->Blockbrowser inside the wallet. It is a wip, so expect more functions to come to it.

You can use a proxy server for connecting to the network, e.G. Tor proxy. Read the .../kekchain/doc/tor.md for instructions.

Download the KeKChat to keep contact and distribute your red pills here: https://github.com/kekchain-project/KeKChat/releases
_____________________________________________________________________________________________________________________________________________________________________________________________

### PATCH NOTES

v0.1.3.0
- updated checkpoints
- updated readme
- github setup

v0.1.2.0
- added "Download KeKChat" button to the overviewpage
- added "Update your node list" to the peer debug window
- updated README.md
- set the SYMBOL_WIDTH in the overviewpage to 175

v0.1.1.0
- added "Download custom backgrounds" button to overviewpage

v0.1.0.0
- release
_____________________________________________________________________________________________________________________________________________________________________________________________

### KEKCHAIN DISTRIBUTION

There will be airdrops on 4chan for KeK distribution purposes. Download and install the Kekchain Core Wallet, go to 'receive' in the navigation bar and click on 'Request Payment' to get a new Kekchain address. Post your address in the airdrop thread and you will receive some. Alternatively, you can post on 4chan and use your Kekchain address as username or insert it at the bottom of your post. You can use a new address for every post, so your privacy is not compromissed. Users can tip you KeK to that address when they like your post. This way, you can get a steady income of KeK. Stake them to support the network and earn more KeK. If you have a lot of KeK, please tip people to grow the network. KeK isn't on any exchanges, so the airdrops are helping for better distribution.

You can also send an eMail containing your kekchain wallet address to kekchain-project@mail.de. You will receive some KeK after ~24 hours. For better privacy, you can use a throw away eMail address.

Also, you can use the KeKChat (https://github.com/kekchain-project/KeKChat/releases) and write a message into the public chat with your Kekchain address, the KeKKinG will send you some KeK IF he reads your message.

_____________________________________________________________________________________________________________________________________________________________________________________________

### TROUBLESHOOTING


Installation issues

- always make sure you have installed the dependencies
- if you still can't install, try $ sudo snap install bitcoin-core #missing dependencies can be installed together with the bitcoin-core snap
- use a virtual box or virtual pc with a linux distro if you are a windows user, or build from source on windows (search for "how to build bitcoin from source on windows" guides and adapt to kekchain)


Connection issues:

- go to Settings->Options->Open configuration file, add the following lines and restart your wallet:

	txindex=1
	addnode=84.56.212.76
	addnode=46.16.221.206
	
- go to help->debug window->console and insert the following commands:

	addnode 84.56.212.76 add
	addnode 46.16.221.206 add

- check if your firewall is blocking kekchain-wallet

- open ports 8888 and 8889 for mainnet; 18888 and 18889 for testnet


No mature coins:

- it takes 500 block confirmations for coins to mature for staking (maturity)

_____________________________________________________________________________________________________________________________________________________________________________________________
Copyright (C) 2020 The Kekchain Core developers

Support me: send ETH or any ether-token to 0x23a9cEDec1C8a6B5471e20E4C9cD7b083a4d9cde
