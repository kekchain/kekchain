
Debian
====================
This directory contains files used to package kekchaind/kekchain-qt
for Debian-based Linux systems. If you compile kekchaind/kekchain-qt yourself, there are some useful files here.

## kekchain: URI support ##


kekchain-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install kekchain-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your kekchain-qt binary to `/usr/bin`
and the `../../share/pixmaps/bitcoin128.png` to `/usr/share/pixmaps`

kekchain-qt.protocol (KDE)

