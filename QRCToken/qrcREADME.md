# QRC20Token
----------------------

## Create a new token with the online compiler

### Step by step guide

QRC Token = QuantumresistantRedpillCommando Token

Download the offline compiler at https://github.com/kekchain-project/soliditycompiler/releases

Here follows a tutorial for adding existing tokens in your wallet and creating new tokens with the online compiler. I recommend the offline compiler for better privacy. A tutorial for using the offline compiler is included when you download it. Go to your wallet->smart contracts and click on "Download offline compiler" to go to the download page.

QRC Token = QuantumresistantRedpillCommando Token

You can give your Token a redpilling or meme-ish name. The name can be up to 5500 characters long and 5600 Bytes great. No "Enter" and no "'" is allowed in those textes, the compiler will give errors when you use them. There are maybe also other symbols which i havn't testet giving errors.

You can combine multiple Token addressess to get unlimited storage. Cut your redpilling text into different pieces, to not be over the character limit. Give your Tokens a corresponding symbol and follow up on it, e.g. ExampleToken1, ExampleToken2, etc. Save the contract addressess to one text file. Create a new Token, name it ExampleTokenHeader and insert all the contract addressess you have saved earlier into it. Be carefull to keep the right order of the contracts, so the first should be first, last should be last. This way, someone only needs to know the Header address and can copy the other addressess out of it, adding each contract to his wallet, reading the red pills in there or copy and saving the texts to a local text file.

- In the QRCToken folder, open and copy the code of the QRC20Token.sol

- Go to https://remix.ethereum.org/ or click on 'Solidity compiler' in your wallet

- Click on 'create new file', create 'YourTokenName.sol' //short name of your token, same as symbol

- Paste the code from QRC20Token.sol into it

- Adjust the three variables 'name=' 'symbol=' and 'totalSupply=' to your liking and leave the site open //name = 'your red pill here'

- From the QRCToken folder, open and copy the SafeMath.sol code

- Go back to https://remix.ethereum.org/ and click on 'create new file' again, create 'SafeMath.sol'

- Paste the code from SafeMath.sol into it

- Activate the Solidity compiler at the Plugin manager on the left side and switch to 'YourTokenName.sol'

- Click on the Solidity compiler on the left side and choose the 0.4.18 compiler from the drop down menu at the top. Activate auto compiling

- Wait for the compiler to load

- Click and copy on 'Bytecode' at the bottom of the compiler widget, open a local text file and paste the code

- Copy the code between the "" at "object": "STRINGTOCOPY" //->EXAMPLE: 6060604052600860ff1660580a600102600055341561001d57600080fd5b600054600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002081905550610c4f806100726000396000f3006060604052600436106100a4576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806306fdde03146100a9578063095ea7b31461013757806318160ddd1461019157806323b872dd146101ba578063313ce567146102335780635a3b7e421461026257806370a08231146102f057806395d89b411461033d578063a9059cbb146103cb578063dd62ed3e14610425575b600080fd5b34156100b457600080fd5b6100bc610491565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156100fc5780820151818401526020810190506100e1565b50505050905090810190601f1680156101295780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b341561014257600080fd5b610177600480803573ffffffffffffffffffffffffffffffffffffffff169060200190919080359060200190919050506104f1565b604051808215151515815260200191505060405180910390f35b341561019c57600080fd5b6101a46106a1565b6040518082815260200191505060405180910390f35b34156101c557600080fd5b610219600480803573ffffffffffffffffffffffffffffffffffffffff1690602001909190803573ffffffffffffffffffffffffffffffffffffffff169060200190919080359060200190919050506106a7565b604051808215151515815260200191505060405180910390f35b341561023e57600080fd5b610246610987565b604051808260ff1660ff16815260200191505060405180910390f35b341561026d57600080fd5b61027561098c565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156102b557808201518184015260208101905061029a565b50505050905090810190601f1680156102e25780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34156102fb57600080fd5b610327600480803573ffffffffffffffffffffffffffffffffffffffff169060200190919050506109c5565b6040518082815260200191505060405180910390f35b341561034857600080fd5b6103506109dd565b6040518080602001828103825283818151815260200191508051906020019080838360005b83811015610390578082015181840152602081019050610375565b50505050905090810190601f1680156103bd5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34156103d657600080fd5b61040b600480803573ffffffffffffffffffffffffffffffffffffffff16906020019091908035906020019091905050610a16565b604051808215151515815260200191505060405180910390f35b341561043057600080fd5b61047b600480803573ffffffffffffffffffffffffffffffffffffffff1690602001909190803573ffffffffffffffffffffffffffffffffffffffff16906020019091905050610bc7565b6040518082815260200191505060405180910390f35b606060405190810160405280602581526020017f4f79207665792c2074686520676f79696d206b6e6f772120536875742069742081526020017f646f776e2100000000000000000000000000000000000000000000000000000081525081565b60008260008173ffffffffffffffffffffffffffffffffffffffff161415151561051a57600080fd5b60008314806105a557506000600260003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002054145b15156105b057600080fd5b82600260003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055508373ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff167f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925856040518082815260200191505060405180910390a3600191505092915050565b60005481565b60008360008173ffffffffffffffffffffffffffffffffffffffff16141515156106d057600080fd5b8360008173ffffffffffffffffffffffffffffffffffffffff16141515156106f757600080fd5b61077d600260008873ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205485610bec565b600260008873ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002081905550610846600160008873ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205485610bec565b600160008873ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055506108d2600160008773ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205485610c05565b600160008773ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055508473ffffffffffffffffffffffffffffffffffffffff168673ffffffffffffffffffffffffffffffffffffffff167fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef866040518082815260200191505060405180910390a36001925050509392505050565b600881565b6040805190810160405280600981526020017f546f6b656e20302e31000000000000000000000000000000000000000000000081525081565b60016020528060005260406000206000915090505481565b6040805190810160405280600681526020017f4f7953687574000000000000000000000000000000000000000000000000000081525081565b60008260008173ffffffffffffffffffffffffffffffffffffffff1614151515610a3f57600080fd5b610a88600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205484610bec565b600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002081905550610b14600160008673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205484610c05565b600160008673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055508373ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff167fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef856040518082815260200191505060405180910390a3600191505092915050565b6002602052816000526040600020602052806000526040600020600091509150505481565b6000818310151515610bfa57fe5b818303905092915050565b6000808284019050838110151515610c1957fe5b80915050929150505600a165627a7a72305820bbcf3ab0acf9cae7f15f138cb6fa1aceb96f89a24fa1f5adfa920343ca08bbbc0029

- With slow CPUs it can take a while, because the code is a really long string

- in your wallet, go to Smart contracts and click on create contract

- Paste the copied code into the Bytecode field

- Choose a Sender address at the bottom (the address needs to hold some KeK to pay for gas fees) and click 'create contract'

- Copy the 'Sender address' and the 'Contract address' to a local text file and safe it

- Wait for the transaction to be mined

- In your wallet, click on 'QRC Token' and then on 'Add Token'

- Insert the Contract address into the corresponding field and choose the Token address you used to create the contract from the drop down menu, click on confirm

- If this is your first tokeninteraction you need to go to Settings->Options->Main and enable Log events. Restart your wallet after that, it will resync the headers

- Now you can send your token at 'QRC Tokens' -> 'Send' to anyone with a Kekchain address

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Add an existing token

### Step by step guide

- To add an existing token you need the contract address of that token. The creator of that token should publish it somewhere.

- If someone has send tokens from that contract to you, you need to know the receiving address to get access to the tokens. The sender should communicate that to you. You don't need any Token to be able to read the Token Name (The red pill)

- In your wallet, go to 'QRC Tokens' -> 'Add Token'

- Insert the Contract address into the corresponding field and choose a Token address. Click on confirm.

- If this is your first tokeninteraction you need to go to Settings->Options->Main and enable Log events. Restart your wallet after that, it will resync the headers

- You can repeat those steps to add multiple addresses for one token or add more tokens to one or multiple addresses.

- For a test, try to add the following token contract address: 6b1c9350482c38e248e95dacb73b891fa6614f9d #That is the CoroSympHeader contract, containing the contracts for files showing the symptoms of the corona virus. Add every of those contracts to your wallet to get the full text. Use your own header contracts the same way.
